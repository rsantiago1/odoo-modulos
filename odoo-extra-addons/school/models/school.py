
from odoo import fields, models

class SchoolProfile(models.Model):
    _name = "school.profile" # Create database the name: "school_profile"
    _rec_name = "name"

    name = fields.Char(string="Nombre de la escuela")
    address = fields.Char(string="Dirección")
#     email = fields.Char(string="Email")
#     phone = fields.Char(string="Phone")
    # selc = fields.Many2one([
    #     ('normal', 'In Progress'),
    #     ('done', 'Ready for next stage'),
    #     ('blocked', 'Blocked')], string="State")


#     etapas_vida = fields.Selection([
#         ('et01','Adulto'),
#         ('et02','Cachorro'),
#         ('et03','Senior'),
#         ('et04','Todos')], string="Etapas de Vida")
#     tamano_raza = fields.Selection([
#         ('tam01','Gigantes'),
#         ('tam02','Pequeños'),
#         ('tam03','Medianos'),
#         ('tam04','Grandes'),
#         ('tam05','Toy'),
#         ('tam06','Todas')], string="Tamaño de raza")
#     talla = fields.Selection([
#         ('talla01','XS'),
#         ('talla02','otros')], string="Talla")
#     color = fields.Selection([
#         ('color01','Rojo'),
#         ('color02','otros')], string="Color")
#     peso_mascota = fields.Selection([
#         ('peso01','2-3 Kgs.'),
#         ('peso02','3-5 Kgs.'),
#         ('peso03','otros')], string="Peso de la Mascota")


#     influencers = fields.Selection([
#         ('infl01','Influencer 01'),
#         ('infl02','Influencer 02'),
#         ('infl03','Influencer 03')], string="Influencer", default="infl01")

    # Entregas del producto
#     en_tienda = fields.Boolean(string='Entrega en tienda')
#     deliv_mismo = fields.Boolean('Delivery mismo día')
#     deliv_program = fields.Boolean(string='Delivery programado')

#     suscrp = fields.Boolean(string='Permite suscripción')

#     many_test = fields.Many2one("school.school", string="Product")
