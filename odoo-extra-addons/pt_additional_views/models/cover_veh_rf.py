# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CoverVehRf(models.Model):
    _inherit = 'project.task' # Modelo heredado

    placa_2 = fields.Char("PLACA", readonly=True)  # Campo Placa
    fecha_1 = fields.Date("FECHA")  # Campo Fecha
    ot = fields.Many2one("mrp.workorder", string="OT", readonly=True)
    equipo = fields.Text("EQUIPO")  # Campo Fecha
    tipo_de_servicio = fields.Selection([('Normal','Normal')], string="TIPO DE SERVICIO") # Campo Fecha
    compresor = fields.Char("COMPRESOR")  # Campo Fecha
    gas = fields.Char("GAS")  # Campo Fecha
    voltaje = fields.Char("VOLTAJE")  # Campo Fecha
    tecnico_a_cargo = fields.Many2one("hr.employee", string="TECNICO A CARGO")
    temperatura_de_trabajo = fields.Char("TEMPERATURA DE TRABAJO")  # Campo Fecha
    fecha_y_hora_3 = fields.Datetime("FECHA Y HORA")  # Campo Fecha
    fecha_y_hora_2 = fields.Datetime("FECHA Y HORA")  # Campo Fecha
    supervisor_1 = fields.Many2one("hr.employee", string="SUPERVISOR")
    codigo_de_barras_n_ot = fields.Binary("CODIGO DE BARRAS N° OT")  # Campo Fecha
    observaciones_2 = fields.Text("Observaciones")  # Campo Observaciones
