# -*- coding: utf-8 -*-

from odoo import models, fields, api

class InventaryRf(models.Model):
    _inherit = 'project.task' # Modelo heredado

    orden_trabajo = fields.Many2one("mrp.workorder", string="Orden de Trabajo", readonly=True)
    placa_inv = fields.Char("Placa")

    antena_1 = fields.Boolean("Antena")
    espejo_ext_1 = fields.Boolean("Espejo Ext")
    brazo_limp_1 = fields.Boolean("Brazo Limp.")
    plumillas_1 = fields.Boolean("Plumillas")

    espejo_interior_1 = fields.Boolean("Espejo Interior")
    encendedor_1 = fields.Boolean("Encendedor")
    radio_1 = fields.Boolean("Radio")
    sobre_pisos_1 = fields.Boolean("Sobre Pisos")

    botiqun_1 = fields.Boolean("Botiquín")
    tringulo_1 = fields.Boolean("Triángulo")
    conos_1 = fields.Boolean("Conos")
    extintor_1 = fields.Boolean("Extintor")

    gata_1 = fields.Boolean("Gata")
    llave_rueda_1 = fields.Boolean("Llave rueda")
    rueda_repuesto_1 = fields.Boolean("Rueda Repuesto")
    herramientas_1 = fields.Boolean("Herramientas")

    tarjeta_de_prop_1 = fields.Boolean("Tarjeta de Prop.")
    soat_1 = fields.Boolean("SOAT")
    manual_1 = fields.Boolean("Manual")

    otros_1 = fields.Char("Otros")
    otros_2 = fields.Char("Otros")
    otros_3 = fields.Char("Otros")

    supervisor_a_cargo_1 = fields.Char("Supervisor a cargo")
    fecha_y_hora_1 = fields.Datetime("Fecha y hora")
    rastro_de_dao_en_el_vehculo_1 = fields.Binary("Rastros de daño en el vehículo")
    obs_1 = fields.Text("Obs.")

    nombre_y_apellido_1 = fields.Char("Nombre y apellido")
    dni_1 = fields.Char("DNI")
    firma_1 = fields.Binary("Firma")

    antena_2 = fields.Boolean("Antena")
    espejo_ext_2 = fields.Boolean("Espejo Ext")
    brazo_limp_2 = fields.Boolean("Brazo Limp.")
    plumillas_2 = fields.Boolean("Plumillas")

    espejo_interior_2 = fields.Boolean("Espejo Interior")
    encendedor_2 = fields.Boolean("Encendedor")
    radio_2 = fields.Boolean("Radio")
    sobre_pisos_2 = fields.Boolean("Sobre Pisos")

    botiqun_2 = fields.Boolean("Botiquín")
    tringulo_2 = fields.Boolean("Triángulo")
    conos_2 = fields.Boolean("Conos")
    extintor_2 = fields.Boolean("Extintor")

    gata_2 = fields.Boolean("Gata")
    llave_rueda_2 = fields.Boolean("Llave rueda")
    rueda_repuesto_2 = fields.Boolean("Rueda Repuesto")
    herramientas_2 = fields.Boolean("Herramientas")

    tarjeta_de_prop_2 = fields.Boolean("Tarjeta de Prop.")
    soat_2 = fields.Boolean("SOAT")
    manual_2 = fields.Boolean("Manual")

    otros_4 = fields.Char("Otros")
    otros_5 = fields.Char("Otros")
    otros_6 = fields.Char("Otros")

    supervisor_a_cargo_2 = fields.Char("Supervisor a cargo")
    fecha_y_hora_2 = fields.Datetime("Fecha y hora")
    rastro_de_dao_en_el_vehculo_2 = fields.Binary("Rastros de daño en el vehículo")
    obs_2 = fields.Text("Obs.")

    nombre_y_apellido_2 = fields.Char("Nombre y apellido")
    dni_2 = fields.Char("DNI")
    firma_2 = fields.Binary("Firma")
