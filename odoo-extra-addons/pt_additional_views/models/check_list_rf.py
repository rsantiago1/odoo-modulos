# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CheckListRf(models.Model):
    _inherit = 'project.task' # Modelo heredado

    presupuesto = fields.Boolean("Presupuesto")
    inventario_check_list_de_ingreso_rf = fields.Boolean("Inventario (check list de ingreso RF)")
    ot_orden_de_trabajo = fields.Boolean("OT (Orden de trabajo)")
    cartilla_de_identificacin_del_vehculo = fields.Boolean("Cartilla de identificación del vehículo")
    informe_tcnico = fields.Boolean("Informe técnico")
    inventario_de_salida = fields.Boolean("Inventario de salida")
    check_list_de_calidad = fields.Boolean("Check list de calidad")
    formato_de_descarga_de_almacn = fields.Boolean("Formato de descarga de almacén")
    factura = fields.Boolean("Factura")
    otros = fields.Boolean("Otros")
    observaciones = fields.Char("Observaciones")