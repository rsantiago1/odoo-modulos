# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CheckListCalRf(models.Model):
    _inherit = 'project.task' # Modelo heredado

    ot_1 = fields.Many2one("mrp.workorder", string="OT", readonly=True)
    fecha_2 = fields.Date("Fecha")
    hora = fields.Char("Hora")
    cliente_1 = fields.Many2one("res.partner", string="Cliente", readonly=True)

    supervisor_2 = fields.Many2one("hr.employee", string="Supervisor")
    tcnico_resp_1 = fields.Many2one("hr.employee", string="Técnico resp.")
    equipo = fields.Selection([('A','A')], string="Equipo")
    tipo_de_serv = fields.Selection([('Normal','Normal')], string="Tipo de serv.")
    compresor_1 = fields.Char("Compresor")
    gas_1 = fields.Char("Gas")
    voltaje_1 = fields.Char("Voltaje")
    temperatura = fields.Char("Temperatura")

    descripcin_del_trabajo = fields.Text("Descripción del trabajo")

    base_en_buen_estado = fields.Boolean("Base en buen estado")
    base_bien_asegurada_al_motor_torque_y_locktite = fields.Boolean("Base bien asegurada al motor (torque y locktite)")
    compresor_asegurado_a_al_abase_torque_y_locktite = fields.Boolean(
        "Compresor asegurado a al abase (torque y locktite)")
    polea_de_compresor_alineada = fields.Boolean("Polea de compresor alineada")
    polea_auxiliar_asegurada = fields.Boolean("Polea auxiliar asegurada")
    polea_tensora = fields.Boolean("Polea tensora")
    faja_en_buen_estado_y_templada_sin_sonidos_de_giro = fields.Boolean(
        "Faja en buen estado y templada, sin sonidos de giro")
    nivel_de_aceite_del_compresor = fields.Boolean("Nivel de aceite del compresor.")
    cantidad_oz = fields.Float("Cantidad Oz.&#9;&#9;")

    tuberias_de_valvula_de_solenoide = fields.Boolean("Tuberias de valvula de  solenoide")
    tuberias_de_valvula_de_expansion = fields.Boolean("Tuberias de valvula de expansion")
    tuberia_nro_8_condensador_al_furgon = fields.Boolean("Tuberia Nro 8 (condensador al furgon)")
    tuberia_nro_10_evaporador_al_furgon = fields.Boolean("Tuberia Nro 10 (evaporador al furgon)")
    manguera_nro_8_furgon_al_compresor = fields.Boolean("Manguera Nro 8 ( furgon al compresor)")
    manguera_nro_10_furgon_al_compresor = fields.Boolean("Manguera Nro 10 ( furgon al compresor)")
    tuberias_y_mangueras_sin_fugas = fields.Boolean("Tuberias y mangueras sin fugas")
    proteccion_de_tuberia_de_baja_armaflex = fields.Boolean("Proteccion de tuberia de baja (armaflex)")
    proteccion_de_mangueras_coil = fields.Boolean("Proteccion de mangueras (coil)")

    pernos_de_condensador_al_furgon_asegurados = fields.Boolean("Pernos de condensador al furgon asegurados")
    perforaciones_siliconeadas = fields.Boolean("Perforaciones siliconeadas")
    condensador_centrado_y_nivelado = fields.Boolean("Condensador centrado y nivelado")
    asegurado_pernos_de_carcasa = fields.Boolean("Asegurado pernos de carcasa")
    calcomania_de_marca = fields.Boolean("Calcomania de marca")

    funcionamiento_de_ventiladores_de_condensador = fields.Boolean("Funcionamiento de ventiladores de condensador&#9;")
    funcionamiento_de_ventiladores_de_evaporador = fields.Boolean("Funcionamiento de ventiladores de evaporador")
    instalacion_del_panel_de_control = fields.Boolean("Instalacion del panel de control")
    instalacion_de_caja_de_relays = fields.Boolean("Instalacion de caja de relays")
    instalacion_de_cable_de_contacto = fields.Boolean("Instalacion de cable de contacto")
    instalacion_de_cable_positivo_y_a_tierra = fields.Boolean("Instalacion de cable positivo y a tierra")
    harnes_electrico_asegurado = fields.Boolean("Harnes electrico asegurado")

    pernos_del_evaporador_al_furgon_asegurados = fields.Boolean("Pernos del evaporador al furgon asegurados&#9;")
    perforaciones_siliconeadas_1 = fields.Boolean("Perforaciones siliconeadas")
    evaporador_centrado = fields.Boolean("Evaporador centrado")
    asegurar_pernos_de_carcasa = fields.Boolean("Asegurar pernos de carcasa")
    instalacion_de_manguera_de_drenaje = fields.Boolean("Instalacion de manguera de drenaje")

    instalacion_de_cortinas_de_pvc = fields.Boolean("Instalacion de cortinas de PVC")
    instalacion_jebe_de_pico_de_pato = fields.Boolean("Instalacion jebe de pico de pato")

    presurizacion_con_nitrogeno = fields.Boolean("Presurizacion con nitrogeno")
    vacio = fields.Boolean("Vacio.")
    vacio_tiempo = fields.Char("Vacio: Tiempo")
    carga_de_gas = fields.Boolean("Carga de gas.")
    carga_de_gas_cantidad = fields.Char("Carga de gas: Cantidad")
    tiempo_de_prueba = fields.Boolean("Tiempo de prueba")
    tiempo_de_prueba_1 = fields.Char("Tiempo de prueba")
    informe_entregado = fields.Boolean("Informe entregado")
    se_cumplio_con_la_hora_y_fecha_de_entrega = fields.Boolean("Se cumplio con la hora y fecha de entrega?")
    calcomania_de_proximo_servicio = fields.Boolean("Calcomania de proximo servicio")

    instalacion_de_canaleta = fields.Boolean("Instalacion de canaleta")
    retiro_de_todas_la_herramientas = fields.Boolean("Retiro  de todas la herramientas.")
    retiro_de_equipos_y_materiales = fields.Boolean("Retiro de equipos y materiales")
    limpieza_interna_de_la_cabina = fields.Boolean("Limpieza interna de la cabina")
    limpieza_externa_de_la_cabina = fields.Boolean("Limpieza externa de la cabina")
    limpieza_interna_del_furgon = fields.Boolean("Limpieza interna del furgon")
    limpieza_externa_del_furgon = fields.Boolean("Limpieza externa del furgon")

    firma_del_cliente_1 = fields.Binary("Firma del cliente")
    firma_del_tecnico_responsible = fields.Binary("Firma del tecnico responsible")
    firma_del_supervisor = fields.Binary("Firma del Supervisor")
